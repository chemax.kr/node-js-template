var socket = io.connect();
let seenArr = [];

socket.on('error', function (error) {
    console.log(error);
});

socket.on('cronjobState', function (data) {
    console.log(data);
})

socket.on('listfilesall', function (data) {
    // console.log(data);
    console.log('listFiles');
    for (let i in data.files)
    {
        let file = list.table.files.filter(function (item) {
            return item.name === list.table.files[i].name;
        });
        if(file.length > 0)
        {
            data.files[i].seen = file[0].seen;
        }

    }

    list.table.headers = data.headers;
    list.table.files = data.files;
    // files = [];
    // data.forEach(function (item, index, arr) {
    //     files.push({name: item, id: index});
    // });
    // filesForAll.elements = data;
});

// socket.on('', function (dataset) {
//
// })

let list = new Vue({
    el: "#maintable",
    data: {
		activeClass: 'not-hidden-location',
		errorClass: 'hidden-location',
        datetime: "",
        table: {
            headers: [
                // {name: "test"}, {name: "test3"}, {name: "test2"}, {name: "test32"}
                ],
            files: [
            //     {
            //     name: "test6",
            //     folders: [{name: "test"}, {name: "test3", checked: true}, {name: "test2"}, {name: "test32"}]
            // }, {name: "test5", folders: [{}, {}, {}, {checked: true}]}, {
            //     name: "test4",
            //     folders: [{checked: true}, {}, {}, {checked: true}]
            // }
            ]
        }

    },
    methods: {
		changeHidden: function(file){
		    // console.log(this);
            // console.log(e.target.isActive);
            console.log(file);
			file.seen = !file.seen;

		},
        sync: function () {
          socket.emit('sync');
        },
        checkboxToggle: function (file, folder) {
            console.log(file.name, folder.name, folder.checked);
            let dataset = {name: file.name, folder: folder.name, action: folder.checked};
            socket.emit('fileStateChange', dataset)

        },
        deleteFileFromInd: function (file) {
            console.log(file);
            socket.emit('deleteFileFromInd', file.name);
        },
        deleteAllFileExempl: function (file) {
            console.log(file);
            socket.emit('deleteAllFileExempl', file.name);
        },
        addTimer: function (file) {
            console.log(file.name, file.datetime);
            socket.emit('addCronJob', {file: file.name, job: file.datetime});
        },
        deleteTimer: function (file) {
            socket.emit('delCronJob', {file: file.name});
        }
    }
});
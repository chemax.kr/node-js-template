var files = [
    // {id: 1, name: 'Angular', description: 'Superheroic JavaScript MVW Framework.', price: 100},
    // {id: 2, name: 'Ember', description: 'A framework for creating ambitious web applications.', price: 100},
    // {id: 3, name: 'React', description: 'A JavaScript Library for building user interfaces.', price: 100}
];

function findfile(fileId) {
    return files[findfileKey(fileId)];
};

function folderList(fileId) {

}

function findfileKey(fileId) {
    for (var key = 0; key < files.length; key++) {
        if (files[key].id == fileId) {
            return key;
        }
    }
};




var List = Vue.extend({
    template: '#file-list',
    data: function () {
        return {files: files, searchKey: ''};
    },
    computed: {
        filteredfiles: function () {
            return this.files.filter(function (file) {
                return this.searchKey == '' || file.name.indexOf(this.searchKey) !== -1;
            }, this);
        }
    },
    methods: {
        deletefile: function (e) {
            // let file = this.file;
            console.log(this.file);
        }
    }

});

var file = Vue.extend({
    template: '#file',
    data: function () {
        return {file: findfile(this.$route.params.file_id), folderList: ""};
    },

});


var fileDelete = Vue.extend({
    template: '#file-delete',
    data: function () {
        // console.log(findfile(this.$route.params.file_id));
        return {file: findfile(this.$route.params.file_id)};

    },
    methods: {
        deletefile: function () {
            console.log('try to delete');
            console.log(this.$route.params.file_id);
            file = findfile(this.$route.params.file_id);


            router.push('/');
            socket.emit('deleteFile', file.name);
            // files.splice(findfileKey(this.$route.params.file_id), 1);

        }
    }
});

var Addfile = Vue.extend({
    template: '#add-file',
    data: function () {
        return {file: {name: '', description: '', price: ''}}
    },
    methods: {
        createfile: function () {
            var file = this.file;
            files.push({
                id: Math.random().toString().split('.')[1],
                name: file.name,
                description: file.description,
                price: file.price
            });
            router.push('/');
        }
    }
});

var router = new VueRouter({
    routes: [
        {path: '/', component: List},
        {path: '/file/:file_id', component: file, name: 'file'},
        {path: '/add-file', component: Addfile},
        // { path: '/file/:file_id/edit', component: fileEdit, name: 'file-edit'},
        {path: '/file/:file_id/delete', component: fileDelete, name: 'file-delete'}
    ]
});
app = new Vue({
    router: router
}).$mount('#app')
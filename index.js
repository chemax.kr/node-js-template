/*========*/
const config = require('./libs/config');
const fs = require('fs');
const fileUpload = require('express-fileupload');
const express = require('express');
const basicAuth = require('express-basic-auth');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const bodyParser = require('body-parser');
let CronJob = require('cron').CronJob;
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const shell = require('shelljs');
/*========*/

/*========*/
const adapter = new FileSync(config.get('lowdbpath'));
const db = low(adapter);
db.defaults({ jobs: []})
    .write();

setInterval(checkOldJobs, 600000);

function checkOldJobs() {
    console.log('Планы: ',db.get('jobs').value());
    let checkArr = db.get('jobs').value();
    for(let i in checkArr){
        let date = Date.parse(checkArr[i].job);
        if(((date - Date.now()) /1000/60) < -10 ){
            deleteFileFromInd("", checkArr[i].file);
            modifyCronDb(checkArr[i], 'del')
        }
    }
}

let job;
let jobsArr = [];
let jobsParamArr;

setTimeout(function () {
    jobsParamArr = db.get('jobs').value();
    // console.log(jobsParamArr);
    for (let z in jobsParamArr)

    {
        // console.log(jobsParamArr[z]);
        addCronJob("", jobsParamArr[z]);
    }
},
    3000);


// console.log('jobsArr', jobsArr);


const urlencodedParser = bodyParser.urlencoded({extended: false});
const jsonParser = bodyParser.json();
/*========*/

server.listen(config.get('port'), function () {
    console.log('listening on *:' + config.get('port'));
});

/*===========*/
function startSync()
{
    console.log('start sync');
    shell.exec(config.get('syncscript-path'),  {async:true});
}

function addCronJob(socket, dataset) {

    if ((dataset.file) && (dataset.job)) {

        let date = new Date(dataset.job.replace("T", " "));

        jobsArr[dataset.file] = new CronJob(date, function () {
            console.log('try to delete', dataset.file);
            deleteFileFromInd("", dataset.file);
            modifyCronDb(dataset, 'del');
        }, function () {

        },
            true
        );


        // jobsArr[dataset.file] = job;
        // console.log('successfuly create ', jobsArr);
        modifyCronDb(dataset, 'add');

    }
    else {
        modifyCronDb(dataset, 'del');
    }
    // io.sockets.emit('cronjobState', jobsArr);
    // console.log('jobsArr', jobsArr);
}

function modifyCronDb(dataset, action) {

    db.get('jobs').remove({file: dataset.file}).write();
    if(action === 'add')
    {
        // db.get('jobs').remove({file: dataset.file}).write();
        db.get('jobs')
            .push(dataset)
            .write()
    }

    sendCronState();

}

function sendCronState() {

    getListFiles("");
    // io.sockets.emit('cronjobState', db.get('jobs'));
}

function delCronJob(socket, dataset) {
    // console.log(dataset);
    if (jobsArr[dataset.file] !== void 0) {
        jobsArr[dataset.file].stop();
    }
    else {

    }
    modifyCronDb(dataset, 'del');

}

function getUnauthorizedResponse(req) {
    return req.auth ?
        ('Credentials ' + req.auth.user + ':' + req.auth.password + ' rejected') :
        'No credentials provided'
}

function deleteAllFileExempl(socket, filename) {
    let path = config.get('upload-path-all') + filename;
    if (fs.existsSync(path)) {
        fs.unlinkSync(path);

    }
    deleteFileFromInd(socket, filename);
}

function deleteFileFromInd(socket, filename) {
    fs.readdir(config.get('path-ind'), function (err, folder) {
        for (i in folder) {
            let path = config.get('path-ind') + folder[i] + config.get('pathslash') + filename;
            if (fs.existsSync(path)) {
                fs.unlinkSync(path);
            }
        }
        getListFiles(socket)
    });
}

function deleteFile(socket, data) {
    let path = config.get('path-ind') + data.folder + config.get('pathslash') + data.name;
    if (fs.existsSync(path)) {
        fs.unlinkSync(path);
        getListFiles(socket)
    }
}

function copyFile(socket, data) {
    let path = config.get('path-ind') + data.folder + config.get('pathslash') + data.name;
    let place = config.get('upload-path-all') + data.name;
    if (!fs.existsSync(path)) {
        fs.linkSync(place, path);

    }
    getListFiles(socket);
}

function getListFiles(socket) {
    let table = {};
    table.files = [];
    table.headers = [];
    fs.readdir(config.get('path-ind'), function (err, folder) {
        for (i in folder) {
            table.headers.push({name: folder[i]});
        }
        fs.readdir(config.get('upload-path-all'), function (err, data) {
            for (i in data) {
                let folders = [];
                let count = 0;
                for (c in table.headers) {
                    let path = config.get('path-ind') + table.headers[c].name + config.get('pathslash') + data[i];
                    if(fs.existsSync(path))
                    {
                        count ++;
                    }
                    folders.push({name: table.headers[c].name, checked: fs.existsSync(path)});

                    // console.log(table.headers[c].name);
                }

                let datetime = db.get('jobs').find({file: data[i]}).value();
                if(typeof datetime === 'undefined')
                {
                    datetime = {job: ""};
                }
                table.files.push({name: data[i], folders: folders, datetime: datetime.job, count: count});
                // datetime: db.get('jobs').find({name: data[i] }).value()
            }
            console.log('send listfilesall');
            if (socket !== "") {
                io.sockets.emit('listfilesall', table);

            }
            else {
                io.sockets.emit('listfilesall', table);
            }
            // sendCronState();
        });

    });


}


function uploadFiles(req, res) {
    // console.log(req);
    console.log(Object.keys(req.files).length);
    if (Object.keys(req.files).length < 1){
        console.log('no files');
        res.redirect('/');
        // res.status(400).send('No files were uploaded.');

    }
    else{



    // console.log(req.files);
    let sampleFile = req.files.sampleFile;

    // Use the mv() method to place the file somewhere on your server
    sampleFile.mv(config.get('upload-path-all') + sampleFile.name, function (err) {
        if (err)
            return res.status(500).send(err);

        res.status(200);
        // res.send(sampleFile.name);
        res.redirect('/');
    });
    }
}

/*===========*/

/*========*/
app.use(basicAuth({
    users: {'admin': 'supersecret'},
    challenge: true,
    unauthorizedResponse: getUnauthorizedResponse
}));
app.use(express.static(__dirname + '/public'));
app.use(fileUpload());
app.post('/uploadAll', function (req, res) {
    uploadFiles(req, res);
});

app.post('/uploadInd', function (req, res) {
    uploadFiles(req, res);
});


/*========*/

io.on('connection', function (socket) {

    console.log('a user connected');
    socket.on('sync',function () {
        startSync();
    });
    socket.on('test', function (dataset) {
        console.log('test', dataset);
        socket.emit('test', 'successfull');
    });

    socket.on('delCronJob', function (dataset) {
        delCronJob(socket, dataset);
    });

    socket.on('addCronJob', function (dataset) {
        addCronJob(socket, dataset);
    });

    socket.on('fileStateChange', function (dataset) {

    });
    socket.on('deleteFileFromInd', function (data) {
        deleteFileFromInd(socket, data);

    });
    socket.on('deleteAllFileExempl', function (data) {
        deleteAllFileExempl(socket, data);

    });
    socket.on('fileStateChange', function (data) {
        console.log(data);
        if (!data.action) {
            deleteFile(socket, data);
        }
        else {
            copyFile(socket, data);
        }

    });
    socket.on('getlistfiles', function () {
        getListFiles(socket);
        setTimeout(getListFiles, 3000, socket);
        // setTimeout(getListFiles(socket), 3000);

    })
});